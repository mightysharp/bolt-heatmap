# WakeHeatmap
Adds heatmap tracking to Bolt pages. Required JavaScripts, CSS, and HTML Snippets are injected using `Bolt\Asset` classes.

## TODO Features
  - [x] Multi-page heatmapping
  - [ ] White / Black list in settings page (currently hard-coded)
  - [ ] Async data functions for aggregate heat view (improve performance on sluggish browsers)



## Requirements
  - TypeScript
  - SCSS
  - JQuery
  - Heatmap.js


## Repositories
WakeHeatmap lives in two repos, a public release and a private development, so it will become necessary to manage multiple remotes and ensure you are pushing and pulling from the correct ones.

NOTE: The repositories are currently out of sync. The public repo is the current living version.
