<?php

namespace Bolt\Extension\WakeCreative\WakeHeatmap\Controller;

use Bolt\Extension\WakeCreative\WakeHeatmap\Storage;

use Bolt\Controller\Base;

use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use DateTime;


class HeatmapAjaxController extends Base {
  // Handles ajax requests from the heatmap library.
  // Responsible for saving heat data to db, and REST handlers for the data analytics backend view.

  public function addRoutes(ControllerCollection $colle) {
    // $colle->match('/', [$this, 'cbHeatmapAdmin']);

    $colle->match('/hello', [$this, 'cbHeatmapAdmin']);

    // Heatmap admin page
    // $colle->match('/extensions/heatmap', [$this, 'cbHeatmapAdmin']);


    $colle->match('/submit-session-data', [$this, 'cbSubmitSessionData']);
    $colle->match('/get-page-sessions', [$this, 'cbGetPageSessions']);
    $colle->match('/get-page-sessions-count', [$this, 'cbGetPageSessionsCount']);

    // Aggregate
    $colle->match('/get-aggregate-data', [$this, 'cbGetAggregateData']);


    return $colle;
  }



  // ==============
  // Callbacks
  // ==============




  public function cbGetAggregateData() {
    if (!isset($_REQUEST['devicewidth'])) {
      $html = 'Missing devicewidth in request data.';
      return new Response($html, Response::HTTP_BAD_REQUEST);
    }
    if (!isset($_REQUEST['pageurl'])) {
      $html = 'Missing pageurl in request data.';
      return new Response($html, Response::HTTP_BAD_REQUEST);
    }

    $devicewidth = filter_var($_REQUEST['devicewidth'], FILTER_SANITIZE_NUMBER_INT);
    $pageurl = filter_var($_REQUEST['pageurl'], FILTER_SANITIZE_URL);

    if (empty($devicewidth) || is_null($devicewidth) || empty($pageurl) || is_null($pageurl)) {
      $html = 'Failed to extract devicewidth or pageurl from request data. You likely have a malformed request.';
      return new Response($html, Response::HTTP_BAD_REQUEST);
    }

    $repo = $this->app['storage']->getRepository(Storage\WakeHeatmapEntity::class);

    $records = $repo->getAggregateData($devicewidth, $pageurl);
    for ($i = 0; $i < count($records); $i++) {
      $records[$i]['map'] = json_decode($records[$i]['map']);
    }

    return new Response(json_encode($records), Response::HTTP_OK);
  }



  public function cbGetPageSessionsCount() {
    if (!isset($_REQUEST['pageurl'])) {
      $html = "Missing pageurl in request data.";
      return new Response($html, Response::HTTP_BAD_REQUEST);
    }

    $pageurl = filter_var($_REQUEST['pageurl'], FILTER_SANITIZE_URL);

    $repo = $this->app['storage']->getRepository(Storage\WakeHeatmapEntity::class);

    $count = $repo->getPageSessionsCount($pageurl);
    return new Response($count, Response::HTTP_OK);
  }



  // Returns a json object of sessions for the requested page
  public function cbGetPageSessions() {
    if (!isset($_REQUEST['pageurl'])) {
      $html = "Missing pageurl in request data.";
      return new Response($html, Response::HTTP_BAD_REQUEST);
    }

    $pageurl = filter_var($_REQUEST['pageurl'], FILTER_SANITIZE_URL);

    $repo = $this->app['storage']->getRepository(Storage\WakeHeatmapEntity::class);

    $sessions = $repo->getPageSessions($pageurl);
    return new Response(json_encode($sessions), Response::HTTP_OK);
  }




  public function cbSubmitSessionData() {
    // save the session data to the database

    // ==============
    // BAILOUTS
    // ==============

    if (!isset($_REQUEST['session'])) {
      $html = "Missing session object in request data.";
      return new Response($html, Response::HTTP_BAD_REQUEST);
    }

    if (
          !isset($_REQUEST['session']['md5'])
      ||  !isset($_REQUEST['session']['date'])
      ||  !isset($_REQUEST['session']['user_agent'])
      ||  !isset($_REQUEST['session']['duration'])
      ||  !isset($_REQUEST['session']['deviceWidth'])
      ||  !isset($_REQUEST['session']['deviceHeight'])
      ||  !isset($_REQUEST['session']['heatmap'])
      ||  !isset($_REQUEST['session']['pageURL'])
    ) {
      $html = "Missing session data.";
      return new Response($html, Response::HTTP_BAD_REQUEST);
    }

    // Sanitize data
        
    $data = [
      'md5' => filter_var($_REQUEST['session']['md5'], FILTER_SANITIZE_STRING),
      'date' => new DateTime(filter_var($_REQUEST['session']['date'], FILTER_SANITIZE_STRING)),
      'useragent' => filter_var($_REQUEST['session']['user_agent'], FILTER_SANITIZE_STRING),
      'duration' => filter_var($_REQUEST['session']['duration'], FILTER_SANITIZE_STRING),
      'devicewidth' => filter_var($_REQUEST['session']['deviceWidth'], FILTER_SANITIZE_STRING),
      'deviceheight' => filter_var($_REQUEST['session']['deviceHeight'], FILTER_SANITIZE_STRING),
      'map' => filter_var_array($_REQUEST['session']['heatmap'], FILTER_SANITIZE_STRING),
      'pageurl' => filter_var($_REQUEST['session']['pageURL'], FILTER_SANITIZE_STRING),
    ];


    $em = $this->app['storage'];
    $repo = $em->getRepository(Storage\WakeHeatmapEntity::class);
    
    $entity = $repo->create($data);
    $result = $repo->save($entity);

    return new Response($result, Response::HTTP_OK);
  }





}
