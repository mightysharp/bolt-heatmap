<?php

namespace Bolt\Extension\WakeCreative\WakeHeatmap\Storage;

use Bolt\Storage\Entity;
use Bolt\Storage\Repository;
use Doctrine\DBAL\Query\QueryBuilder;


class WakeHeatmapRepository extends Repository {

  private $_heatmap_sessions = [];



  // TODO: REMOVE DEPRECATED TESTING FN
  // public function getAggregateSessions() {
  //   $qb = $this->createQueryBuilder()
  //     ->select('DISTINCT devicewidth as width')
  //     ->addSelect('COUNT(devicewidth) as count')
  //     ->groupBy('devicewidth')
  //     ->orderBy('COUNT(devicewidth)', 'DESC');

  //   $records = $qb->execute()->fetchAll();
  //   return $records;
  // }



  public function getPageSessions($page) {
    $qb = $this->createQueryBuilder()
      ->select('DISTINCT devicewidth as width')
      ->addSelect('COUNT(devicewidth) as count')
      ->where('pageurl = "'.$page.'"')
      ->groupBy('devicewidth')
      ->orderBy('COUNT(devicewidth)', 'DESC');

    $records = $qb->execute()->fetchAll();
    return $records;
  }



  public function getPageSessionsCount($page) {
    $qb = $this->createQueryBuilder()
      ->select('COUNT(devicewidth) as count')
      ->where('pageurl = "'.$page.'"');

    $count = $qb->execute()->fetch();
    return (int)$count['count'];
  }



  public function getAggregateData($devicewidth, $page) {
    $qb = $this->createQueryBuilder()
      ->select('*')
      ->where('devicewidth = "'.$devicewidth.'"')
      ->andWhere('pageurl = "'.$page.'"')
    ;

    $data = $qb->execute()->fetchAll();
    return $data;
  }





  // public function getSessionsCount() {
  //   $qb = $this->createQueryBuilder()
  //     ->select('COUNT(devicewidth) as count');

  //   $count = $qb->execute()->fetch();
  //   return (int)$count['count'];
  // }


  public function getPageUrls() {
    $qb = $this->createQueryBuilder()
      ->select('DISTINCT pageurl as page')
      ->orderBy('pageurl', 'ASC');

    $pages = $qb->execute()->fetchAll();
    $list = [];
    foreach ($pages as $page) {
      $list = array_merge($list, array_values($page));
    }

    return $list;
  }








  // Testing functions

  // public function getAllSessions() {
  //   $qb = $this->createQueryBuilder()
  //     ->select('*');

  //   $records = $this->findWith($qb);
  //   return $records;
  // }


  // public function getSessionsByDeviceWidth($device_width) {
  //   // stub
  // }

  // public function getSessionsByPageUrl($page_url) {
  //   // stub
  // }

}
