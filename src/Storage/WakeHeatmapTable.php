<?php

namespace Bolt\Extension\WakeCreative\WakeHeatmap\Storage;

use Bolt\Storage\Database\Schema\Table\BaseTable;


class WakeHeatmapTable extends BaseTable {

  protected function addColumns() {
    $this->table->addColumn('id', 'integer', [ 'autoincrement' => true ]);
    $this->table->addColumn('md5', 'guid', [ 'notnull' => true ]);
    $this->table->addColumn('date', 'datetime', [ 'notnull' => true ]);
    $this->table->addColumn('useragent', 'text', [ 'notnull' => true ]);
    $this->table->addColumn('duration', 'float', [ 'notnull' => true ]);
    $this->table->addColumn('devicewidth', 'integer', [ 'notnull' => true ]);
    $this->table->addColumn('deviceheight', 'integer', [ 'notnull' => true ]);
    $this->table->addColumn('map', 'json', [ 'notnull' => true ]);
    $this->table->addColumn('pageurl', 'text', [ 'notnull' => true ]);
  }

  protected function addIndexes() {
    $this->table->addIndex([
      'md5', 'date', 'devicewidth',
    ]);
  }

  protected function setPrimaryKey() {
    $this->table->setPrimaryKey([ 'id', ]);
  }

}
