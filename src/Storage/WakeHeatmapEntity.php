<?php

namespace Bolt\Extension\WakeCreative\WakeHeatmap\Storage;

use Bolt\Storage\Entity\Entity;


// Represents a heatmap session
class WakeHeatmapEntity extends Entity {
  // Is a basic container object 

  public $id;
  public $md5;
  public $date;
  public $useragent;
  public $duration;
  public $devicewidth;
  public $deviceheight;
  public $map;
  public $pageurl;

}
