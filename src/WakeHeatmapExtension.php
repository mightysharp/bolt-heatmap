<?php

namespace Bolt\Extension\WakeCreative\WakeHeatmap;

use Bolt\Extension\SimpleExtension;
use Bolt\Extension\DatabaseSchemaTrait;
use Bolt\Extension\StorageTrait;

use Bolt\Storage\Repository;

// Bolt\Asset
use Bolt\Asset\Target;
use Bolt\Asset\File\JavaScript;
use Bolt\Asset\File\Stylesheet;
use Bolt\Asset\Snippet\Snippet;
// menu
use Bolt\Menu\MenuEntry;

use Silex\Application;
use Silex\ControllerCollection;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use DateTime;

/**
 * ExtensionName extension class.
 *
 * @author Your Name <you@example.com>
 * 
 * TODO: use config.yml settings for routes and such
 */
class WakeHeatmapExtension extends SimpleExtension {

  use DatabaseSchemaTrait;
  use StorageTrait;


  // TODO: get value from config
  private $heatmapRoute = '/extensions/heatmap';


  protected function registerServices(Application $app) {
    $this->extendDatabaseSchemaServices();
    $this->extendRepositoryMapping();
  }

  // Controllers

  // NOTE: there are no endpoints or routes that should use heatmap data on the front end
  // protected function registerFrontendControllers() {
  //   return [
  //     '/heatmap' => new Controller\HeatmapAjaxController(),
  //   ];
  // }

  // all heatmap routes are {{bolt_admin}}/heatmap
  protected function registerBackendControllers() {
    return [
      $this->heatmapRoute => new Controller\HeatmapAjaxController(),
    ];
  }







  // Storage Layer
  protected function registerExtensionTables() {
    return [
      'wake_heatmap' => Storage\WakeHeatmapTable::class,
    ];
  }

  protected function registerRepositoryMappings() {
    return [
      'wake_heatmap' => [ Storage\WakeHeatmapEntity::class => Storage\WakeHeatmapRepository::class ],
    ];
  }





  // Menu Items
  protected function registerMenuEntries() {
    $menu = MenuEntry::create('heatmap-admin-menu', 'heatmap')
      ->setLabel('Heatmap Admin')
      ->setIcon('fa:pie-chart')
      ->setPermission('settings');
      // ->setRoute('HeatmapExtension');

    return [
      $menu,
    ];
  }

  // This should probably be in a Controller class
  protected function registerBackendRoutes(ControllerCollection $colle) {
    $colle->get('/extensions/heatmap', [$this, 'cbHeatmapAdmin']);
  }

  public function cbHeatmapAdmin(Application $app, Request $request) {
    $context = [];

    $entity_manager = $this->container['storage'];
    $repo = $entity_manager->getRepository(Storage\WakeHeatmapEntity::class);


    // TODO:

    $context['sessions']['pages'] = $repo->getPageUrls();
    // testing
    // $context['sessions']['count'] = $repo->getSessionsCount();
    // $context['sessions']['aggregates'] = $repo->getAggregateSessions();

    // $context['pageses'] = $repo->getPageSessions('http://localhost:8080/');
    // $context['pagecount'] = $repo->getPageSessionsCount('http://localhost:8080/pages');


    // $context['sessions'] = (array)$repo->getAllSessions();

    $html = $this->renderTemplate('HeatmapAdmin.twig', $context);
    return new Response($html, Response::HTTP_OK);
  }






  // Assets

  protected function registerAssets(){
    // NOTE: For web assets to be loaded on local ext dev you must copy them to the bolt public extensions directory

    $assets = [];

    if (isset($_REQUEST['heatmap'])) {
      $assets[] = Snippet::create()
        ->setCallback([$this, 'cbHeatDataViewStyles'])
        ->setLocation(Target::AFTER_CSS)
        ->setPriority(100)
      ;
    }

    // snippets
    // $heat_container = Snippet::create()
    $assets[] = Snippet::create()
      ->setCallback([$this, 'cbHeatContainerSnippet'])
      ->setLocation(Target::BEFORE_BODY_JS)
      ->setPriority(0);


    // styles
    // $heat_styles = Stylesheet::create()
    $assets[] = Stylesheet::create()
      ->setFileName('css/heatmap.css')
      ->setLate(false)
      ->setPriority(0);

    // javascripts

    // crypto js cdn
    // $crypto_js_core = JavaScript::create()
    $assets[] = JavaScript::create()
      ->setFileName('https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/core.min.js')
      ->setLate(true)
      ->setPriority(0)
      ->setAttributes(['defer']);
    // $crypto_js_md5 = JavaScript::create()
    $assets[] = JavaScript::create()
      ->setFileName('https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/md5.min.js')
      ->setLate(true)
      ->setPriority(1)
      ->setAttributes(['defer']);

    // $heatmap_js = JavaScript::create()
    $assets[] = JavaScript::create()
      ->setFileName('vendor/heatmap.js')
      ->setLate(true)
      ->setPriority(9)
      ->setAttributes(['defer']);

    // TODO: Update lib to async load, needs to check if its dependencies are loaded
    // $heatmap_config = JavaScript::create()
    $assets[] = JavaScript::create()
      ->setFileName('js/heatmap_config.js')
      ->setLate(true)
      ->setPriority(10)
      ->setAttributes(['defer']);

    return $assets;
  }


  public function cbHeatDataViewStyles() {
    ob_start();
    ?>
    <style>
      #heat-wrap {
        opacity: 1 !important;
        z-index: 9999;
      }
    </style>
    <?php
    return ob_get_clean();
  }


  public function cbHeatContainerSnippet() {
    return '<div id="heat-wrap"><div id="heatmap-container"></div></div>';
  }

}
