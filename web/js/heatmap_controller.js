"use strict";
var postEndpoint = '';
(function () {
    // NOTE: regex only works for normal domains, not ip addresses
    var postEndpointArr = /((http|https):\/{2}((\w|-)+\.)*\w*(\w*:\d{4})?\/?){1}/g.exec(window.location.href);
    if (postEndpointArr === null) {
        throw new Error("postEndpointArr is null");
    }
    postEndpoint = postEndpointArr[0];
})();
// FIXME: this should get from config file or a a better regex
// Hard coded bolt controller endpoint
postEndpoint += 'bolt/extensions/heatmap/';
console.log(postEndpoint);
var pageSelect = jQuery("#page-select");
var sessionTable = jQuery("#aggregate-sessions-table");
var aggregateView = jQuery("#heat-view");
if (pageSelect.length == 0 || sessionTable.length == 0 || aggregateView.length == 0) {
    throw new Error("pageSelect or sessionTable or aggreagteView not found.");
}
jQuery(document).ready(function ($) {
    // Populate select with page urls
    // ajaxPopulatePageUrls(); // NOTE: we are now doing this in the twig
    pageSelect.change(onChangeSelect);
    // NOTE: we are waiting for user input before populating anything
    // pageSelect.change();
});
// DONE
// DEPRECATED
function ajaxPopulatePageUrls() {
    jQuery.ajax(postEndpoint + 'ajax.php', {
        type: 'POST',
        async: false,
        data: {
            ajax_action: 'pages-for-hm',
        }
    })
        .fail(function (response) {
        console.log(response);
        throw new Error("Failed to retrieve site pages");
    })
        .done(function (response) {
        var jsonResponse = JSON.parse(response);
        jsonResponse.forEach(function (element, ndx) {
            // console.log(element);
            pageSelect.append(new Option(element.pageURL, element.pageURL));
        });
    });
}
function ajaxPopulateTable() {
    //obtain device widths and sessions counts 
    //console.log("Obtaining device widths and session counts for " + pageSel.options[0].value);
    // New
    var selectPageUrl = pageSelect.val();
    if (selectPageUrl === null) {
        throw new Error("No page url is selected.");
    }
    console.log("Page Select URL: " + selectPageUrl);
    var totalSessions = 0;
    // get the total sessions for the selected page url
    jQuery.ajax(postEndpoint + 'get-page-sessions-count', {
        type: 'POST',
        async: false,
        data: {
            pageurl: selectPageUrl,
        }
    })
        .fail(function (response) {
        console.log(response);
        alert("Failed to retrieve total sessions count for page.");
    })
        .done(function (response) {
        // console.log(response);
        totalSessions = response;
    });
    // Prevent divide by zero error
    if (totalSessions === 0) {
        var msg = "No sessions found for selected page.";
        console.log(msg);
        alert(msg);
        return;
    }
    // Get sessions table data
    jQuery.ajax(postEndpoint + 'get-page-sessions', {
        type: 'POST',
        async: false,
        data: {
            // ajax_action: 'device-width-count',
            // device_width: '', // Gets all device widths
            pageurl: selectPageUrl,
        }
    })
        .fail(function (response) {
        console.log(response);
        alert("Failed to retrieve device width count");
    })
        .done(function (response) {
        var jsonResponse = JSON.parse(response);
        var table = sessionTable[0];
        jsonResponse.forEach(function (element) {
            var tr = table.insertRow(-1);
            var width = tr.insertCell(-1);
            var sessionsCount = tr.insertCell(-1);
            var pctUsers = tr.insertCell(-1);
            var btn = tr.insertCell(-1);
            var percentUsers = element.count / totalSessions;
            width.textContent = element.width.toString();
            sessionsCount.textContent = element.count.toString();
            pctUsers.textContent = (100 * percentUsers).toFixed(2) + '%';
            // The button element
            var _btnEl = jQuery('<button>', {
                attr: {
                    'data-device-width': element.width,
                    'data-sessions-count': element.count,
                    'data-percent-users': percentUsers,
                },
                text: 'Load Data'
            })
                .click(loadHeatData);
            btn.appendChild(_btnEl[0]);
        });
    });
}
function loadHeatData(event) {
    var _this = jQuery(this);
    // console.log(_this.data('device-width'));
    var deviceWidth = _this.data('device-width');
    var containerWidth = aggregateView.parent().width() || 0;
    // if (typeof(containerWidth) === 'undefined') {
    //   throw new Error("Couldn't get iframe container width.")
    // }
    // BUG: sometimes the scale propery is set to 1 when it should be a smaller value
    // this happens when switch from one desktop view to another desktop view
    var iframeScale = containerWidth / deviceWidth;
    if (iframeScale > 1)
        iframeScale = 1; // normalize scale
    // console.log(iframeScale);
    var queryArgs = '&devicewidth=' + deviceWidth.toString()
        + '&pageurl=' + pageSelect.val();
    // aggregateView is iframe
    aggregateView.attr('src', pageSelect.val() + '?heatmap' + queryArgs);
    aggregateView.width(deviceWidth);
    // aggregateView.height('100%');
    aggregateView.css('transform', 'scale(' + iframeScale.toString() + ')');
}
// Utility function
// TODO: test me
// NOTE: not using
function getSessionsData(page, deviceWidth) {
    if (page === void 0) { page = ''; }
    if (deviceWidth === void 0) { deviceWidth = 0; }
    // No valid device or page width
    if (page.length == 0)
        return false;
    jQuery.ajax(postEndpoint + 'ajax.php', {
        type: 'POST',
        async: true,
        data: {
            ajax_action: 'device-width-count',
            device_width: deviceWidth > 0 ? deviceWidth : '',
            pageURL: page,
        }
    })
        .fail(function (response) {
        console.log(response);
        console.log("Failed to retrieve sessions for page and device width");
        return false;
    })
        .done(function (response) {
        // TODO: build a sessions object and return it
        return response;
    });
}
function onChangeSelect(event) {
    //clear table
    clearTable();
    //clear view
    clearView();
    //query data
    // query the data for the selected page
    // use data to populate table
    ajaxPopulateTable();
    //build view
    // get aggregate data and pass that to iframe or whatever
}
// TODO: stub
function clearView() {
    // Reset url to current select url
    // Set aggregate data to first device width
}
function clearTable() {
    var table = sessionTable[0];
    // we're modifying a table while iterating over it so we always remove 1st index and never increment i
    // FIXME: this makes more sense as a while loop (1 < table.rows.length)
    for (var i = 1; i < table.rows.length;) {
        table.deleteRow(i);
    }
    return table;
}
//# sourceMappingURL=heatmap_controller.js.map