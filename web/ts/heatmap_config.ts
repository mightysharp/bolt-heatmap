
// Global variable city


// query args
var queryArgs: {[key: string]: string} = {};
(window.onpopstate = function (this: void) {
  let match: RegExpExecArray|null,
    pl     = /\+/g,  // Regex for replacing addition symbol with a space
    search = /([^&=]+)=?([^&]*)/g,
    decode = function (s: string) { return decodeURIComponent(s.replace(pl, " ")); },
    query  = window.location.search.substring(1);

  while (match = search.exec(query))
    queryArgs[decode(match[1])] = decode(match[2]);
})();



var userIP = '';
var blacklist = [
  // list of ip address we don't want to track heatmapping on
  '104.201.104.106', // mightysharp
  // cer
  '75.98.103.106',
  '75.98.103.107',
  '75.98.103.108',
  '75.98.103.109',
  '75.98.103.110',
];



// Get post endpoint from window href
var postEndpoint: string = '';
(() => {
  let exArr = /((http|https):\/{2}((\w|-)+\.)*\w*(\w*:\d{4})?\/?){1}/g.exec(window.location.href);
  if (exArr !== null) postEndpoint = exArr[0];
})()





var duration = 0;
var heatmap: h337.Heatmap<string, string, string>;
var sessionDate = new Date().toISOString();
// var deviceInfo = null;
var deviceWidth: number;
var deviceHeight: number;
//get page's url, for selecting page to display corresponding heatmap
var pageURL = window.location.href;
var sessionMD5 = CryptoJS.MD5(navigator.userAgent + sessionDate + pageURL).toString();

var body = document.getElementsByTagName('body')[0];

// console.log(postEndpoint);



// Async wait for jquery
// Everything below requires jquery
(function jqueryAsyncWait () {
  var attempt = 0;
  if (typeof(jQuery) === 'undefined') {
    attempt++;
    if (attempt == 100) return;

    window.setTimeout(jqueryAsyncWait, 1000);
  }
  else onJqueryReady();
})();


function onJqueryReady() {
  
  // Get the users ip
  (function () {
    jQuery.ajax('https://api.ipify.org/?format=json', {
      dataType: 'json',
      async: false
    })
      .fail(function (response) {
        console.log("Failed to get users ip address");
      })
      .done(function (response) {
        userIP = response.ip;
      });
  })();





  jQuery(document).ready(function () {

    deviceWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
    deviceHeight = (window.innerHeight > 0) ? window.innerHeight : screen.height;
  
    // Init heatmap
  
    var heatmapContainer = document.getElementById('heatmap-container');
    if (heatmapContainer === null) {
      console.log("Could not find #heatmap-container element.");
      return;
    }
  
    var heatConfig = {
      container: heatmapContainer,
      // maxOpacity: .6,
      radius: 50,
      blur: .90,
      // backgroundColor with alpha so you can see through it
      backgroundColor: 'rgba(0, 0, 58, 0.8)'
    };
  
    heatmap = h337.create(heatConfig);
    heatmap.setDataMin(0);
    heatmap.setDataMax(10);
    // NOTE: Adjust weights to tune visualization
    // heatmap.setDataMax(30);
  
  
    // Setup events
    if (!("heatmap" in queryArgs) && !(blacklist.indexOf(userIP) > -1)) {
      // Register heat tracking events

      body.onmousemove = body.ontouchmove = function(event: TouchEvent|MouseEvent) {
        let x:number = -1, y:number = -1;
        if (event instanceof MouseEvent) {
          x = event.pageX;
          y = event.pageY;
        }
        else if (event instanceof TouchEvent) {
          if (event.touches) {
            x = event.touches[0].pageX;
            y = event.touches[0].pageY;
          }
        }
        else {
          throw new Error("Invalid event type of mousemove or touchevent.");
        }

        if (x < 0 || y < 0) return;
        heatmap.addData({ x: x, y: y, value: 1 });
      };
    
      body.onclick = body.ontouchend = function (event: TouchEvent|MouseEvent) {
        let x:number = -1, y:number = -1;
        if (event instanceof MouseEvent) {
          x = event.pageX;
          y = event.pageY;
        }
        else if (event instanceof TouchEvent) {
          if (event.touches) {
            x = event.touches[0].pageX;
            y = event.touches[0].pageY;
          }
        }
        else {
          throw new Error("Invalid event type of mousemove or touchevent.");
        }

        if (x < 0 || y < 0) return;
        heatmap.addData({ x: x, y: y, value: 5 });
      };
    
      // Send post request with data
      window.addEventListener('unload', function (event) {
        saveData();
      });

    }
    else if ("heatmap" in queryArgs) {

      console.log("Building aggregate data... (This may take some time)");
      // Visualizing aggregate heat data
      console.log("post endpoint: " + postEndpoint);
      $.post(postEndpoint + 'bolt/extensions/heatmap/get-aggregate-data', {
        // ajax_action: "aggregate-heat-data",
        devicewidth: queryArgs.devicewidth,
        pageurl: queryArgs.pageurl
      })
        .fail(function (response) {
          console.log(response);
          alert("Failed to retrieve aggregated heat data.");
        })
        .done(function (response) {
          // FIXME: types here are ambiguous because I didn't know the shape of the jsonResponse data.
          console.log(response);
          var jsonResponse = JSON.parse(response);
          var aggregateData = [];
          jsonResponse.forEach(function (item:any) {
            // TODO: not sure about this syntax
            // aggregateData = aggregateData.concat(item["map"]["data"]);
            //console.dir(item);
            heatmap.addData(item["map"]["data"]);
            //console.dir(item);
          });
        });
  
    }
  
  
    // Start the session over and re init heatmap
    window.addEventListener('resize', function (event) {
      // magic num 5 seconds to prevent multiple submissions of the same data. really just needs to something significantly large
      // TODO: check if there is a significant amound of data worth submitting
      if (duration > 5 && !("heatmap" in queryArgs)) saveData();

      duration = 0;
      deviceWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
      deviceHeight = (window.innerHeight > 0) ? window.innerHeight : screen.height;
  
      // destroy existing heatmap
      if (heatmapContainer === null) return;
      while (heatmapContainer.firstChild) {
        heatmapContainer.removeChild(heatmapContainer.firstChild);
      }
  
      // Recreate heatmap
      heatmap = h337.create(heatConfig);
      heatmap.setDataMin(0);
      heatmap.setDataMax(30);
  
      // TODO: Load new aggregate data
    });
  
    // FIXME: Don't use an interval timer. just get start time and compare it to time in saveData
    // Setup timer
    duration = 0;
    setInterval(function () {
      duration++;
    }, 1000);
  });

}



var saveData = function () {
  console.log("Submitting session data");
  if (!heatmap) {
    console.log ("No heatmap instance");
    console.log(heatmap);
    return;
  }


  var heatData = heatmap.getData();

  if (heatData["data"].length > 0) {
    // FIXME: hardcoded bolt dependency
    jQuery.ajax(postEndpoint + 'bolt/extensions/heatmap/submit-session-data', {
      type: 'POST',
      // async: false,
      data: {
        // ajax_action: 'save-heat-data',
        "session": {
          "md5": sessionMD5,
          "date": sessionDate,
          "user_agent": navigator.userAgent,
          "duration": duration,
          "deviceWidth": deviceWidth,
          "deviceHeight" : deviceHeight,
          "heatmap": heatData,
          "pageURL": pageURL
        }
      },
    })
      .done(function (response) {
        // console.log("Done response:")
        console.log(response)
      })
      .fail(function (response) {
        console.log("Failed to submit heatmapping data.");
        console.dir(response);
      });
  }
};
