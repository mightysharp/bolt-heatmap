var postEndpoint = '';
(function () {
  // NOTE: regex only works for normal domains, not ip addresses
  var postEndpointArr = /((http|https):\/{2}((\w|-)+\.)*\w*(\w*:\d{4})?\/?){1}/g.exec(window.location.href);
  if (postEndpointArr === null) {
    throw new Error("postEndpointArr is null");
  }
  postEndpoint = postEndpointArr[0];
})();

// FIXME: this should get from config file or a a better regex
// Hard coded bolt controller endpoint
postEndpoint += 'bolt/extensions/heatmap/';
console.log(postEndpoint);



var pageSelect = jQuery("#page-select") as JQuery<HTMLSelectElement>;
var sessionTable = jQuery("#aggregate-sessions-table") as JQuery<HTMLTableElement>;
var aggregateView = jQuery("#heat-view") as JQuery<HTMLIFrameElement>;

if (pageSelect.length == 0 || sessionTable.length == 0 || aggregateView.length == 0) {
  throw new Error("pageSelect or sessionTable or aggreagteView not found.");
}


jQuery(document).ready($ => {

  // Populate select with page urls
  // ajaxPopulatePageUrls(); // NOTE: we are now doing this in the twig

  pageSelect.change(onChangeSelect);
  // NOTE: we are waiting for user input before populating anything
  // pageSelect.change();

});

// DONE
// DEPRECATED
function ajaxPopulatePageUrls() {
  jQuery.ajax(postEndpoint + 'ajax.php', {
    type: 'POST',
    async: false,
    data: {
      ajax_action: 'pages-for-hm',
    }
  })
    .fail(function (response) {
      console.log(response);
      throw new Error("Failed to retrieve site pages");
    })
    .done(function (response) {
      let jsonResponse = JSON.parse(response);
      jsonResponse.forEach((element: { pageURL: string }, ndx: number) => {
        // console.log(element);
        pageSelect.append(new Option(element.pageURL, element.pageURL));
      });
    });
}








function ajaxPopulateTable() {
  //obtain device widths and sessions counts 
  //console.log("Obtaining device widths and session counts for " + pageSel.options[0].value);

  // New
  let selectPageUrl = pageSelect.val();
  if (selectPageUrl === null) {
    throw new Error("No page url is selected.");
  }


  console.log("Page Select URL: " + selectPageUrl);


  let totalSessions = 0;
  // get the total sessions for the selected page url
  jQuery.ajax(postEndpoint + 'get-page-sessions-count', {
    type: 'POST',
    async: false,
    data: {
      pageurl: selectPageUrl,
    }
  })
    .fail(response => {
      console.log(response);
      alert("Failed to retrieve total sessions count for page.");
    })
    .done(response => {
      // console.log(response);
      totalSessions = response as number;
    });

  // Prevent divide by zero error
  if (totalSessions === 0) {
    let msg = "No sessions found for selected page.";
    console.log(msg);
    alert(msg);
    return;
  }




  // Get sessions table data
  jQuery.ajax(postEndpoint + 'get-page-sessions', {
    type: 'POST',
    async: false,
    data: {
      // ajax_action: 'device-width-count',
      // device_width: '', // Gets all device widths
      pageurl: selectPageUrl,
    }
  })
    .fail(function (response) {
      console.log(response);
      alert("Failed to retrieve device width count");
    })
    .done(function (response) {
      let jsonResponse = JSON.parse(response);
      let table = sessionTable[0] as HTMLTableElement; 

      jsonResponse.forEach(
        (element: { width: number, count: number }) => {
          let tr = table.insertRow(-1);
          let width = tr.insertCell(-1);
          let sessionsCount = tr.insertCell(-1);
          let pctUsers = tr.insertCell(-1); 
          let btn = tr.insertCell(-1);
          
          let percentUsers = element.count / totalSessions;

          width.textContent = element.width.toString();
          sessionsCount.textContent = element.count.toString();
          pctUsers.textContent = (100 * percentUsers).toFixed(2) + '%';

          // The button element
          let _btnEl = jQuery('<button>', {
            attr: {
              'data-device-width': element.width,
              'data-sessions-count': element.count,
              'data-percent-users': percentUsers,
            },
            text: 'Load Data'
          })
            .click(loadHeatData);

          btn.appendChild(_btnEl[0]);
      });
    });
}






function loadHeatData(this: HTMLElement, event: JQuery.Event) {
  let _this = jQuery(this);  
  // console.log(_this.data('device-width'));

  let deviceWidth: number = _this.data('device-width');  
  let containerWidth = aggregateView.parent().width() || 0;
  // if (typeof(containerWidth) === 'undefined') {
  //   throw new Error("Couldn't get iframe container width.")
  // }

  // BUG: sometimes the scale propery is set to 1 when it should be a smaller value
  // this happens when switch from one desktop view to another desktop view
  let iframeScale = containerWidth / deviceWidth;
  if (iframeScale > 1) iframeScale = 1; // normalize scale

  // console.log(iframeScale);

  let queryArgs
    = '&devicewidth=' + deviceWidth.toString()
    + '&pageurl=' + pageSelect.val();

  // aggregateView is iframe
  aggregateView.attr('src', pageSelect.val() + '?heatmap' + queryArgs);
  aggregateView.width(deviceWidth);
  // aggregateView.height('100%');
  aggregateView.css('transform', 'scale(' + iframeScale.toString() + ')');
}




// Utility function
// TODO: test me
// NOTE: not using
function getSessionsData(page = '', deviceWidth = 0) {
  // No valid device or page width
  if (page.length == 0) return false;

  jQuery.ajax(postEndpoint + 'ajax.php', {
    type: 'POST',
    async: true,
    data: {
      ajax_action: 'device-width-count',
      device_width: deviceWidth > 0 ? deviceWidth : '',
      pageURL: page,
    }
  })
    .fail(response => {
      console.log(response);
      console.log("Failed to retrieve sessions for page and device width");
      return false;
    })
    .done(response => {
      // TODO: build a sessions object and return it
      return response;
    });
}







function onChangeSelect(event: JQuery.Event) {

  //clear table
  clearTable();

  //clear view
  clearView();

  //query data
  // query the data for the selected page
  // use data to populate table
  ajaxPopulateTable();

  //build view
  // get aggregate data and pass that to iframe or whatever


}



// TODO: stub
function clearView() {
  // Reset url to current select url
  // Set aggregate data to first device width
}




function clearTable() {
  let table = sessionTable[0] as HTMLTableElement;
  // we're modifying a table while iterating over it so we always remove 1st index and never increment i
  // FIXME: this makes more sense as a while loop (1 < table.rows.length)
  for (let i = 1; i < table.rows.length;) {
    table.deleteRow(i);
  }
  return table;
}
